var KILOS_TO_POUNDS_RATIO = 2.20462;

Vue.component('row-set', {
  template: '#row-set-template',
  props: ['set', 'index', 'baselineValues'],

  methods: {
    weightValue: function(value) {
      return (value * this.set.percentage) / 100;
    },
  }
})

Vue.component('sets-table', {
  template: '#sets-table-template',
  props: ['sets', 'baseValue', 'metric', 'baselineValues'],

  methods: {
    onClickDeleteRow: function(index) {
      this.$emit('remove-set', index);
    }
  }
})

var app = new Vue({
    el: '#app',
    data: {
      currentBaselineKilosValue: null,
      currentBaselinePoundsValue: null,
      newSetPercentage: null,
      newSetNumReps: null,
      currentTabIndex: 0,
      sets: [],
    },

    computed: {
      poundsSelected: function () {
        return {
          'is-success': this.currentTabIndex === 0,
          'is-selected': this.currentTabIndex === 0,
        }
      },

      kilosSelected: function () {
        return {
          'is-success': this.currentTabIndex === 1,
          'is-selected': this.currentTabIndex === 1,
        }
      },

      bothSelected: function () {
        return {
          'is-success': this.currentTabIndex === 2,
          'is-selected': this.currentTabIndex === 2,
        }
      },
      
      addSetDisabled: function () {
        return this.currentBaselineKilosValue === null 
          || this.currentBaselinePoundsValue === null
          || this.newSetPercentage === null
          || this.newSetNumReps === null;
      },

      currentMetric: function() {
        switch (this.currentTabIndex) {
          case 0:
            return 'pounds';
          case 1:
            return 'kilos';
          case 2:
            return 'both';
        }
      },

      baselineValues: function() {
        let baselineValues = [];
        
        if (this.currentMetric === 'pounds' || this.currentMetric == 'both') {
          baselineValues.push({type: 'pounds', value: this.currentBaselinePoundsValue});
        }

        if (this.currentMetric === 'kilos' || this.currentMetric == 'both') {
          baselineValues.push({type: 'kilos', value: this.currentBaselineKilosValue});
        }

        return baselineValues;
      }
      
    },

    methods: {
      updateValues: function(type, event) {
        var val = parseInt(event.target.value);
        if (val > 0) {
          if (type == "POUNDS") {
            this.currentBaselinePoundsValue = (Math.round(val) === val) ? val : Math.round(val * 2) / 2;
            this.currentBaselineKilosValue = Math.round(val / KILOS_TO_POUNDS_RATIO * 2) / 2;
            
          } else {
            this.currentBaselineKilosValue = (Math.round(val) === val) ? val : Math.round(val * 2) / 2;
            this.currentBaselinePoundsValue = Math.round(val * KILOS_TO_POUNDS_RATIO * 2) / 2; 
          }
        } else {
          this.currentBaselineKilosValue = '';
          this.currentBaselinePoundsValue = '';
        }
      },

      addSet: function() {
        var newSet = { numReps: this.newSetNumReps, percentage: this.newSetPercentage };
        this.sets = this.sets.concat([ newSet ]);
        this.newSetPercentage = null;
        this.newSetNumReps = null;
      },

      onRemoveSet: function(index) {
        this.sets.splice(index, 1);
      },

      switchTabToPounds: function() {
        this.currentTabIndex = 0;
      },

      switchTabToKilos: function() {
        this.currentTabIndex = 1;
      },

      switchTabToShowBoth: function() {
        this.currentTabIndex = 2; 
      }
    }
  })