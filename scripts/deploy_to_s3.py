import boto3
import os
from botocore.exceptions import ClientError

s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')
waiter = s3_client.get_waiter('bucket_exists')
dirname, _ = os.path.split(os.path.abspath(__file__))

try:
    bucket_name = 'weightlifting.jrm2k6.com'
    waiter.wait(
        Bucket=bucket_name,
        WaiterConfig={
            'MaxAttempts': 1
        }
    )
except Exception as e:
    print('Creating bucket with name {}..'.format(bucket_name))
    bucket = s3_resource.Bucket(bucket_name)
    response = bucket.create(
        ACL='public-read',
        CreateBucketConfiguration={
            'LocationConstraint': 'us-west-1'
        },
    )
    print(response)

dest_html_file_name = 'index.html'
dest_js_file_name = 'percentage-calculator.js'
dest_css_file_name = 'styles.css'

src_html_file_name = dirname + '/../' + 'percentage-calculator.html'
src_js_file_name = dirname + '/../' + 'percentage-calculator.js'
src_css_file_name = dirname + '/../' + 'styles.css'

s3_client.upload_file(src_html_file_name, bucket_name, dest_html_file_name, {'ContentType': 'text/html'})
s3_client.upload_file(src_css_file_name, bucket_name, dest_css_file_name, {'ContentType': 'text/css'})
s3_client.upload_file(src_js_file_name, bucket_name, dest_js_file_name, {'ContentType': 'application/javascript'})