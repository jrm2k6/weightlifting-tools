#!/bin/bash

cache_bust=`python -c 'import sys,uuid; sys.stdout.write(uuid.uuid4().hex[0:8])' | pbcopy && pbpaste && echo`

sed -i -- "s/<script src=\"\(.*\).js\">/<script src=\"\1.js?v=$cache_bust\">/" percentage-calculator.html
sed -i -- "s/href=\"\(styles.css\)\"/href=\"\1?v=$cache_bust\"/" percentage-calculator.html

python scripts/deploy_to_s3.py